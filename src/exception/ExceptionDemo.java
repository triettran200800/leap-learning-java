package exception;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ExceptionDemo {
    private void simpleTryCatch(){
        try {
            var reader = new FileReader("file.txt");
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
    }

    private void multipleTryCatch(){
        /* Instead of using three catch, we can try shorter way */

        try {
            var reader = new FileReader("file.txt");
            var value = reader.read();
            new SimpleDateFormat().parse("");

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        /* Shorten try catch (Because IO Exception include FileNotFound Exception) */

        try {
            var reader = new FileReader("file.txt");
            var value = reader.read();
            new SimpleDateFormat().parse("");

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private void usingFinally(){
        /*
            In some case, we use try catch block, we need to do some task when it is success or fail
            To do this we use finally block
            Example: Open file / database connection. We need to close connection when it is success and fail
         */

        FileReader reader = null;
        try {
            reader = new FileReader("file.txt");
            var value = reader.read();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void tryWithResources(){
        /*
            try-with-resources is a shorter way for using finally block in close something (ex: connection, file....)
            it will auto-close any defined connection
         */

        try(
            var reader = new FileReader("file.txt");
            var writer = new FileWriter("writer.txt")
        ){
            var value = reader.read();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    private void usingCustomException(int withdraw, int balance) throws NotEnoughMoneyException {

        if(withdraw > balance){
            throw new NotEnoughMoneyException();
        }
    }

    public void show(){
        System.out.println("Show Exception Demo");

        System.out.println("================ SIMPLE TRY CATCH ================");
        this.simpleTryCatch();

        System.out.println("================ MULTIPLE TRY CATCH ================");
        this.multipleTryCatch();

        System.out.println("================ CUSTOM EXCEPTION ================");
        /* Scenario withDraw money larger than balance in account */
        int withdraw = 30;
        int balance = 20;

        try {
            this.usingCustomException(withdraw, balance);
        } catch (NotEnoughMoneyException e) {
            System.out.printf("Your balance [$%s] is smaller than your withdraw [$%s]%n", balance, withdraw);
            e.printStackTrace();
        }

    }
}
