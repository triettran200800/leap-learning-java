package exception;

public class NotEnoughMoneyException extends Exception {
    public NotEnoughMoneyException(){
        super("Your account is not enough money");
    }

    public NotEnoughMoneyException(String message){
        super(message);
    }
}
