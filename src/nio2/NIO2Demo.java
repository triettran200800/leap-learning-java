package nio2;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NIO2Demo {
    private String demoFoldername = "NIO2_Created";
    private String demoFilename = "temp.txt";

    private void createDirectory(Path p){
        if(Files.exists(p)){
            System.out.println("Directory exist !!!!");
        }
        else{
            try {
                Path finalPath = Files.createDirectories(p);
                System.out.printf("Folder created with path %s\n", finalPath.toString());
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Cannot create folder");
            }

        }

    }

    private void deleteDirectory(Path p){
        try {
            Files.deleteIfExists(p);
            System.out.printf("Deleted folder \n");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Cannot delete folder");
        }
    }

    private void listDirectory(){
        System.out.println("List");
    }

    private void createFile(Path p){
        if(Files.exists(p)){
            System.out.println("File existed");
            return;
        }

        try {
            Path filePath = Files.createFile(p);
            System.out.printf("File created %s\n", filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeFile(Path p){
        var contentList = new ArrayList<String>();

        for (int i = 0; i < 10; i++) {
            contentList.add(String.format("Hello World %s", i));
        }

        try {
            Files.write(p, contentList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFileFuture(Path p){
        try {
            AsynchronousFileChannel asynchronousFileChannel = AsynchronousFileChannel.open(p);
            ByteBuffer buffer = ByteBuffer.allocate(100_000_0);
            Future<Integer> result = asynchronousFileChannel.read(buffer, 0);

            Integer byteLen = result.get();
            System.out.println("Bytes read="+byteLen);
        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void readFileCallBack(Path p){
        try {
            AsynchronousFileChannel asynchronousFileChannel = AsynchronousFileChannel.open(p);
            ByteBuffer buffer = ByteBuffer.allocate(100_000_0);
            asynchronousFileChannel.read(buffer, 0, buffer, new CompletionHandler<Integer, ByteBuffer>() {
                @Override
                public void completed(Integer integer, ByteBuffer byteBuffer) {
                    System.out.println("Bytes read=" + integer);
                }

                @Override
                public void failed(Throwable throwable, ByteBuffer byteBuffer) {
                    System.out.println("Failed to read");
                }
            });

            System.out.println("Waitinggggg");
//            Thread.sleep(1000);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void show(){
        System.out.println("NIO2 Demonstraion");

        var folderPath = Paths.get(this.demoFoldername);

        System.out.println("================ CREATE DIRECTORY ================");
        this.createDirectory(folderPath);

        System.out.println("================ DELETE DIRECTORY ================");
        // this.deleteDirectory(folderPath);

        var filePath = Paths.get(this.demoFoldername, this.demoFilename);

        System.out.println("================ CREATE FILE ================");
        this.createFile(filePath);

        System.out.println("================ WRITE FILE ================");
        this.writeFile(filePath);

        /*
            Read file future
                Property: same as await in JS
                Pros:
                    You absolutely can get back async result when calling future.get() (We have to wait for task complete to get result <=> await in JS)
                Cons:
                    To get result future will block thread
                    So you cannot do any other task if you want to user future result
                    For Example:
                        Future is created to read pdf file
                        After reading pdf file, you have to save content into db
                        This is long-running task and task is invoked by API
                        User don't need immediately result, they only need to be noticed about their PDF file is processing

                        => But you cannot notice user because your API is blocked by future to get PDF content => failed. (You cannot but future.read in other
                        thread because to do that you have to share future between thread)

           To solve the problem of future => we have completable future

           Read file callback:
                Pros: Not block thread
                Cons: Sometime main thread is completed before long-running task => You cannot get expected result
         */

        System.out.println("================ READ FILE FUTURE ================");
        this.readFileFuture(filePath);

        System.out.println("================ READ FILE CALLBACK ================");
        this.readFileCallBack(filePath);
    }

}
