package com.javademo;

import concurrency.ConcurrencyDemo;
import exception.ExceptionDemo;
import generic.GenericDemo;
import nio2.NIO2Demo;
import stream.StreamDemo;

public class Main {
    public static void main(String[] args) {
        /* Exception Demonstration */

        ExceptionDemo exceptionDemo = new ExceptionDemo();
        // exceptionDemo.show();

        /* Generic Demonstration */

        GenericDemo genericDemo = new GenericDemo();
        // genericDemo.show();

        /* Stream Demonstration */

        StreamDemo streamDemo = new StreamDemo();
        // streamDemo.show();

        /* Concurrency Demonstration */
        ConcurrencyDemo concurrencyDemo = new ConcurrencyDemo();
        // concurrencyDemo.show();

        /* NIO2 Demonstration */
        NIO2Demo nio2Demo = new NIO2Demo();
        nio2Demo.show();

    }
}