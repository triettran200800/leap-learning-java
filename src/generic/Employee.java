package generic;

public class Employee implements Comparable{
    private final int salary;

    public Employee(int salary) {
        this.salary = salary;
    }

    @Override
    public int compareTo(Object o) {
        Employee other = (Employee) o;

        if(this.salary == other.getSalary()){
            return 0;
        }

        if(this.salary > other.getSalary()){
            return 1;
        }

        return -1;
    }

    public int getSalary() {
        return salary;
    }
}
