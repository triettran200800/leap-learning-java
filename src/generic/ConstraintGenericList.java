package generic;

public class ConstraintGenericList<T extends Comparable>{
    private final T[] items;
    private int count;

    ConstraintGenericList(){
        this.items = (T[]) new Comparable[10];
        this.count = 0;
    }

    public void add(T element){
        this.items[count++] = element;
    }

    public int length(){
        return this.count;
    }

    public T get(int idx){
        return this.items[idx];
    }
}
