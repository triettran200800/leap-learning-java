package generic;

public class GenericDemo {
    public void show(){
        System.out.println("================ SIMPLE GENERIC LIST ================");

        var simpleStringList = new GenericList<String>();
        simpleStringList.add("a");
        simpleStringList.add("b");

        /* Generic cannot be used with primitive type */
        var simpleIntList = new GenericList<Integer>();
        simpleIntList.add(1);
        simpleIntList.add(2);

        for (int i = 0; i < simpleStringList.length(); i++) {
            System.out.println(simpleStringList.get(i));
        }

        for (int i = 0; i < simpleIntList.length(); i++) {
            System.out.println(simpleIntList.get(i));
        }

        System.out.println("================ CONSTRAINT GENERIC LIST ================");
        /* GenericList Only For Comparable object */
        /*
            With constrain generic it will convert all T class to the first extends
            For examples
                GenericList extends Comparable & Cloneable
                It will convert T to Comparable
        */

        var constraintStringList = new ConstraintGenericList<String>();
        constraintStringList.add("aaa");
        constraintStringList.add("bbb");

        System.out.printf("Compare to result: %s \n", constraintStringList.get(0).compareTo(constraintStringList.get(1)));

        /*
            This constraint generic list will not apply for non-comparable class (such as Employee)
            So if you want to apply to Employee class, you have to implement this Comparable interface
        */

        var constraintEmployeeList = new ConstraintGenericList<Employee>();
        constraintEmployeeList.add(new Employee(1000));
        constraintEmployeeList.add(new Employee(500));

        System.out.printf("Compare to result: %s \n", constraintEmployeeList.get(0).compareTo(constraintEmployeeList.get(1)));
    }
}
