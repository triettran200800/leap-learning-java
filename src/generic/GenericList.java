package generic;

public class GenericList<T> {
    private final T[] items;
    private int count;

    GenericList(){
        this.items = (T[]) new Object[10];
        this.count = 0;
    }

    public void add(T element){
        this.items[count++] = element;
    }

    public int length(){
        return this.count;
    }

    public T get(int idx){
        return this.items[idx];
    }
}
