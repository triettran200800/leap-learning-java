package concurrency;

public class FileDownload implements Runnable {
    private int size;
    private String name;
    private ReportStatus status;

    public FileDownload(int size, String name, ReportStatus status) {
        this.size = size;
        this.name = name;
        this.status = status;
    }

    @Override
    public void run() {
        for (int i = 0; i < size; i++) {
            if(Thread.currentThread().isInterrupted()) return;
            status.incTotalSize();
        }

        System.out.printf("Download complete %s \n", Thread.currentThread().getName());
    }
}
