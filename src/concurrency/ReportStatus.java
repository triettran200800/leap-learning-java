package concurrency;

public class ReportStatus {
    private int totalSize;
    private Object totalSizeMonitor = new Object();

    public int getTotalSize() {
        return totalSize;
    }

    public void incTotalSize(){
        synchronized (this.totalSizeMonitor){
            totalSize+=1;
        }
    }
}
