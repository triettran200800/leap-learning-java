package concurrency;

import java.util.ArrayList;

public class ConcurrencyDemo {
    private ReportStatus status;

    public ConcurrencyDemo(){
        this.status = new ReportStatus();
    }

    public void show(){
        System.out.println("Concurrency Demo");

        System.out.println("================ SIMPLE CONCURRENCY ================");

        /*
            for (int i = 0; i < 10; i++) {
                Thread thread = new Thread(new FileDownload(5, "default.txt"));
                thread.start();
            }
        */

        System.out.println("================ THREAD SAFE CODE ================");

        /*
            In this demonstration, I want to track total of downloaded file size and save this result to ReportStatus class
         */

        var threadList = new ArrayList<Thread>();

        for (int i = 0; i < 1000; i++) {
            Thread thread = new Thread(new FileDownload(5, "default.txt", this.status));
            thread.start();

            threadList.add(thread);
        }

        for (Thread thread: threadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            finally {
                thread.interrupt();
            }
        }

        System.out.println(this.status.getTotalSize());
    }
}
