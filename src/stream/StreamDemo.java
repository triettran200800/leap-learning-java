package stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

public class StreamDemo {
    private void simpleStream(){
        int[] numbers = {1, 2, 3};
        Arrays.stream(numbers).forEach(n -> System.out.println(n));
    }

    public void show(){
        System.out.println("Stream Demo");

        /*
            Stream is a declarative way to

         */

        System.out.println("================ SIMPLE FOR EACH STREAM ================");
        this.simpleStream();

        System.out.println("================ STREAM MAPPING, FILTERING, REDUCING ================");

        var employeeList = new ArrayList<Employee>();

        employeeList.add(new Employee(1000, "Bình", Role.MANAGER));
        employeeList.add(new Employee(500, "Alex", Role.EMPLOYER));
        employeeList.add(new Employee(800, "Curry", Role.MANAGER));

        Optional<Integer> result =  employeeList.stream()
                .map(Employee::getSalary)
                .filter((salary)-> salary > 500)
                .reduce(Integer::sum);

        System.out.println(result.orElse(0));

        System.out.println("================ STREAM SORTING, COLLECTOR, GROUPING, JOINING ================");

        employeeList.stream()
                .sorted(Comparator.comparing(Employee::getSalary))
                .forEach(employee -> System.out.println(employee.getName()));

        var collectionResult = employeeList.stream()
                .collect(Collectors.groupingBy(Employee::getRole, Collectors.mapping(Employee::getName, Collectors.joining(", "))));

        System.out.println(collectionResult);
    }
}
