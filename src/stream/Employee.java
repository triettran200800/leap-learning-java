package stream;

public class Employee implements Comparable {
    private int salary;
    private String name;
    private Role role;

    public Employee(int salary, String name, Role role) {
        this.salary = salary;
        this.name = name;
        this.role = role;
    }

    public Employee(int salary) {
        this.salary = salary;
        this.name = "Default";
    }

    public int getSalary() {
        return salary;
    }

    public String getName() {
        return name;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public int compareTo(Object o) {
        var other = (Employee)o;
        return Integer.compare(this.salary, other.getSalary());
    }
}
